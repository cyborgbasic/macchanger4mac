#!/bin/sh

IF="en1"

echo "mac.changer.4.mac"

echo "Interface chosen: "$IF

OLDMAC=`ifconfig $IF | grep ether | awk 'BEGIN{FS=" "} {print $2}'`

echo "Original MAC address on "$IF" : " $OLDMAC

NEWMAC=`openssl rand -hex 6 | sed 's/\(..\)/\1:/g; s/.$//'`

echo "Generated MAC address for "$IF" :" $NEWMAC

sudo ifconfig $IF down

sudo ifconfig $IF up

read -p "Wait 10 seconds for network card restarting..." -t 10

echo " now changing MAC ..."

sudo ifconfig $IF lladdr $NEWMAC

echo "Checked MAC address:  " `ifconfig $IF | grep ether | awk 'BEGIN{FS=" "} {print $2}'`
