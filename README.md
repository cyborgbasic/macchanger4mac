# macchanger4mac

This simple script is created for MacOSX to change the MAC address
of a given network interface card (NIC) to a random one.

The desired NIC is given as a variable at the top of the script. E.g.:

IF = en1

To make sure that the change succeeds, the script restarts the NIC.
For this, and the following operations administartive credentials are
required, because the commands use sudo.

Finally, the script displays the MAC address of the given NIC, so it is
easy to determine the result.

Changing the MAC address is needed in case you do not want to leave traces
on a local network, e.g. local WiFi. Changing MAC address has no effect to
the log traces you leave on the Internet, since you are NATted.  

Happy MAC-changing on MacOSX, cheers,

Cyborg.Basic
